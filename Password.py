import string
from random import *


class Password:

    def __init__(self):
        self.name = None
        self.value = None

    def create(self, length):
        characters = string.ascii_letters  + string.digits # TODO: string.punctuation eklenecek
        password = "".join(choice(characters) for x in range(length))
        self.value = password

    def get(self):
        return self.value

