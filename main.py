from Password import *
from Database import *
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog,QMainWindow,QLineEdit
import sys
from ui import login,dash,dash_create,dash_update
import pyperclip
class Login(QMainWindow):

    def __init__(self):
        super().__init__()

        self.db = None
        self.selected_id = None

        self.setup_login()

    ## Setup Ui's

    def setup_login(self):
        self.login = login.Ui_MainWindow()
        self.login.setupUi(self)
        self.login.pushButton_browse_db.clicked.connect(self.browsedb)
        self.login.pushButton_open_db.clicked.connect(self.opendb)

        self.login.pushButton_browse_folder.clicked.connect(self.browsefolder)
        self.login.pushButton_create_db.clicked.connect(self.createdb)

        self.login.lineEdit_key_import.setEchoMode(QLineEdit.Password)
        self.login.lineEdit_key_create.setEchoMode(QLineEdit.Password)


        self.dash = dash.Ui_MainWindow()
        self.dash_create = dash_create.Ui_MainWindow()
        self.dash_update = dash_update.Ui_MainWindow()


    def setup_dash(self):
        self.dash.setupUi(self)
        self.dash.pushButton_back_login_screen.clicked.connect(self.back_login)
        self.dash.pushButton_exit.clicked.connect(self.exit)
        self.dash.pushButton_create_pass.clicked.connect(self.create_pass)
        self.dash.pushButton_delete_pass.clicked.connect(self.delete_pass)
        self.dash.pushButton_copy_clipboard.clicked.connect(self.clipboard)
        self.dash.pushButton_regenerate_pass.clicked.connect(self.update_pass_dash)

        self.refreshList()

    def setup_dash_create(self):
        self.dash_create.setupUi(self)
        self.dash_create.pushButton_back_dash.clicked.connect(self.back_dash)
        self.dash_create.pushButton_exit.clicked.connect(self.exit)
        self.dash_create.pushButton_save.clicked.connect(self.save_pass)
        self.dash_create.pushButton_generate_pass.clicked.connect(self.generate_pass)


    def setup_dash_update(self,item):
        self.dash_update.setupUi(self)
        self.dash_update.pushButton_back_dash.clicked.connect(self.back_dash)
        self.dash_update.pushButton_exit.clicked.connect(self.exit)
        self.dash_update.pushButton_save.clicked.connect(self.update_pass)
        self.dash_update.pushButton_generate_pass.clicked.connect(self.update_pass)

        self.dash_update.usernameLineEdit.setText(str(item[1]))
        self.dash_update.passwordLineEdit.setText(str(item[2]))
        self.dash_update.urlLineEdit.setText(str(item[3]))
        self.selected_id = item[0]


    ##Login ui Functions

    #create section

    def browsefolder(self):
        db_path = QFileDialog.getExistingDirectory(None, 'Select Directory')
        self.login.textBrowser_create.setText(db_path)

    def createdb(self):
        db_name = self.login.lineEdit_name_create.text()
        db_password = self.login.lineEdit_key_create.text()
        db_path = self.login.textBrowser_create.toPlainText()
        self.db = Database(db_name, db_password, db_path, "create")
        self.setup_dash()

    #import section

    def browsedb(self):
        db_file = QFileDialog.getOpenFileName(None, 'Select db file', 'Desktop')
        self.login.textBrowser_import.setText(db_file[0])

    def opendb(self):
        db_password = self.login.lineEdit_key_import.text()
        db_file = self.login.textBrowser_import.toPlainText()
        self.db = Database(None, db_password, db_file, "import")
        self.setup_dash()

    ##Dash ui Functions

    def create_pass(self):
        self.setup_dash_create()

    def delete_pass(self):
        db = self.db
        row_no = self.dash.tableWidget.currentRow()
        id = self.dash.tableWidget.item(row_no, 0).text()
        db.delete(id)
        self.refreshList()

    def clipboard(self):
        db = self.db
        row_no = self.dash.tableWidget.currentRow()
        id = self.dash.tableWidget.item(row_no,0).text()
        password = db.get_pass(id)
        pyperclip.copy(str(password[0]))

    def update_pass_dash(self):
        db = self.db
        row_no = self.dash.tableWidget.currentRow()
        id = self.dash.tableWidget.item(row_no,0).text()
        item = db.read(id,"","","")
        self.setup_dash_update(item[0])

    def back_login(self):
        self.setup_login()


    ##Dash_create ui Functions


    def save_pass(self):
        db = self.db
        username = self.dash_create.usernameLineEdit.text()
        password = self.dash_create.passwordLineEdit.text()
        url = self.dash_create.urlLineEdit.text()
        db.write(username,password,url)
        self.setup_dash()

    def generate_pass(self):
        password = Password()
        password.create(16)
        self.dash_create.passwordLineEdit.setText(password.get())


    def back_dash(self):
        self.setup_dash()


    ##Dash_update ui Functions


    def generate_pass_update(self):
        password = Password()
        password.create(16)
        self.dash_create.passwordLineEdit.setText(password.get())
        self.setup_dash()


    def update_pass(self):
        db = self.db
        id = self.selected_id
        username = self.dash_update.usernameLineEdit.text()
        password = self.dash_update.passwordLineEdit.text()
        url = self.dash_update.urlLineEdit.text()
        db.update(id, username, password, url)
        self.setup_dash()



    ##Common Functions

    def refreshList(self):
        db = self.db
        items = db.readAll()
        print(items)
        self.dash.tableWidget.clear()
        self.dash.tableWidget.setRowCount(len(items))
        i = 0
        j = 0
        while i<len(items):
            while j<len(items[i]):
                if j == 2:
                    self.dash.tableWidget.setItem(i, j, QtWidgets.QTableWidgetItem(str("*******")))
                else:
                    self.dash.tableWidget.setItem(i, j, QtWidgets.QTableWidgetItem(str(items[i][j])))
                j+=1
            i+=1
            j=0
        self.dash.tableWidget.setHorizontalHeaderLabels(("id","username","pass","url"))

    def exit(self):
        sys.exit()






if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    prog = Login()
    prog.show()
    sys.exit(app.exec_())