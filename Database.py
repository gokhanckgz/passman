from pysqlcipher3 import dbapi2 as sqlite


class Database:
    def __init__(self, name, db_password,path, type):
        self.name = name
        self.db_pass = db_password
        if type == "import":
            self.path = path
            try:
                with open(path):
                    pass
            except:
                print("sth went wrong")

        elif type == "create":
            self.path = path+'/'+name+'.db'
            print(self.path)
            conn = sqlite.connect(self.path)
            c = conn.cursor()
            c.execute("PRAGMA key=" + db_password + "")
            c.execute('''create table passwords(id INTEGER PRIMARY KEY , name text, password text, url text)''')
            conn.commit()
            c.close()

        else:
            print("what's goin on")



    def read(self,id,name,password,url):
        conn = sqlite.connect(self.path)
        c = conn.cursor()
        c.execute("PRAGMA key=" + self.db_pass + "")
        try:
            sql = '''SELECT * FROM passwords WHERE id = '{}' OR name = '{}' OR password = '{}' OR url = '{}' '''.format(id,name,password,url)
            res = c.execute(sql)
            return (res.fetchall())
        except sqlite.DatabaseError:
            return ("Password Incorrect")
        c.close()

    def readAll(self):
        conn = sqlite.connect(self.path)
        c = conn.cursor()
        c.execute("PRAGMA key=" + self.db_pass + "")
        try:
            sql = '''select * from passwords '''
            res = c.execute(sql)
            return res.fetchall()
        except sqlite.DatabaseError:
            return ("Password Incorrect")
        c.close()



    def write(self,name,password,url):
        conn = sqlite.connect(self.path)
        c = conn.cursor()
        c.execute("PRAGMA key=" + self.db_pass + "")
        sql = '''INSERT INTO passwords(id,name,password,url) VALUES (NULL,'{}','{}','{}') '''.format(name,password,url)
        c.execute(sql)
        conn.commit()
        c.close()



    def delete(self, id):
        conn = sqlite.connect(self.path)
        c = conn.cursor()
        c.execute("PRAGMA key=" + self.db_pass + "")
        sql = '''DELETE FROM passwords WHERE id = '{}' '''.format(id)
        c.execute(sql)
        conn.commit()
        c.close()


    def update(self, id ,username, password,url):
        conn = sqlite.connect(self.path)
        c = conn.cursor()
        c.execute("PRAGMA key=" + self.db_pass + "")
        sql = '''UPDATE passwords SET name = '{}', password = '{}' , url = '{}' WHERE id = '{}' '''.format(username,password,url,id)
        c.execute(sql)
        conn.commit()
        c.close()

    def get_pass(self,id):
        conn = sqlite.connect(self.path)
        c = conn.cursor()
        c.execute("PRAGMA key=" + self.db_pass + "")
        try:
            sql = '''select "password" from passwords where id = '{}' '''.format(id)
            res = c.execute(sql)
            return (res.fetchone())
        except sqlite.DatabaseError:
            return ("Password Incorrect")
        c.close()