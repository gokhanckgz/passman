# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dash_update.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton_generate_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_generate_pass.setGeometry(QtCore.QRect(150, 450, 181, 41))
        self.pushButton_generate_pass.setObjectName("pushButton_generate_pass")
        self.pushButton_back_dash = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_back_dash.setGeometry(QtCore.QRect(20, 20, 141, 41))
        self.pushButton_back_dash.setObjectName("pushButton_back_dash")
        self.pushButton_exit = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_exit.setGeometry(QtCore.QRect(650, 20, 141, 41))
        self.pushButton_exit.setObjectName("pushButton_exit")
        self.pushButton_save = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_save.setGeometry(QtCore.QRect(490, 450, 181, 41))
        self.pushButton_save.setObjectName("pushButton_save")
        self.formLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.formLayoutWidget.setGeometry(QtCore.QRect(160, 130, 511, 299))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout_2 = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout_2.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.formLayout_2.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout_2.setRowWrapPolicy(QtWidgets.QFormLayout.DontWrapRows)
        self.formLayout_2.setLabelAlignment(QtCore.Qt.AlignCenter)
        self.formLayout_2.setFormAlignment(QtCore.Qt.AlignCenter)
        self.formLayout_2.setContentsMargins(50, 50, 50, 50)
        self.formLayout_2.setSpacing(50)
        self.formLayout_2.setObjectName("formLayout_2")
        self.usernameLabel = QtWidgets.QLabel(self.formLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.usernameLabel.setFont(font)
        self.usernameLabel.setObjectName("usernameLabel")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.usernameLabel)
        self.usernameLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.usernameLineEdit.setObjectName("usernameLineEdit")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.usernameLineEdit)
        self.passwordLabel = QtWidgets.QLabel(self.formLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.passwordLabel.setFont(font)
        self.passwordLabel.setObjectName("passwordLabel")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.passwordLabel)
        self.passwordLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.passwordLineEdit.setObjectName("passwordLineEdit")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.passwordLineEdit)
        self.urlLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.urlLabel.setObjectName("urlLabel")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.urlLabel)
        self.urlLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.urlLineEdit.setObjectName("urlLineEdit")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.urlLineEdit)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_generate_pass.setText(_translate("MainWindow", "Generate Password"))
        self.pushButton_back_dash.setText(_translate("MainWindow", "Back to Main Screen"))
        self.pushButton_exit.setText(_translate("MainWindow", "Exit"))
        self.pushButton_save.setText(_translate("MainWindow", "Update"))
        self.usernameLabel.setText(_translate("MainWindow", "username"))
        self.passwordLabel.setText(_translate("MainWindow", "password"))
        self.urlLabel.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:20pt;\">url</span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

