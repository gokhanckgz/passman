# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dash.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setAutoFillBackground(False)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton_create_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_create_pass.setGeometry(QtCore.QRect(70, 460, 111, 41))
        self.pushButton_create_pass.setObjectName("pushButton_create_pass")
        self.pushButton_regenerate_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_regenerate_pass.setGeometry(QtCore.QRect(200, 459, 111, 41))
        self.pushButton_regenerate_pass.setObjectName("pushButton_regenerate_pass")
        self.pushButton_delete_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_delete_pass.setGeometry(QtCore.QRect(331, 459, 111, 41))
        self.pushButton_delete_pass.setObjectName("pushButton_delete_pass")
        self.pushButton_back_login_screen = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_back_login_screen.setGeometry(QtCore.QRect(20, 20, 141, 41))
        self.pushButton_back_login_screen.setObjectName("pushButton_back_login_screen")
        self.pushButton_exit = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_exit.setGeometry(QtCore.QRect(650, 20, 141, 41))
        self.pushButton_exit.setObjectName("pushButton_exit")
        self.pushButton_copy_clipboard = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_copy_clipboard.setGeometry(QtCore.QRect(580, 460, 181, 41))
        self.pushButton_copy_clipboard.setObjectName("pushButton_copy_clipboard")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setEnabled(True)
        self.tableWidget.setGeometry(QtCore.QRect(30, 80, 729, 359))
        self.tableWidget.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableWidget.setShowGrid(False)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        self.tableWidget.horizontalHeader().setVisible(True)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.horizontalHeader().setSortIndicatorShown(False)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setDefaultSectionSize(30)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_create_pass.setText(_translate("MainWindow", "Create Entry"))
        self.pushButton_regenerate_pass.setText(_translate("MainWindow", "Update Entry"))
        self.pushButton_delete_pass.setText(_translate("MainWindow", "Delete Entry"))
        self.pushButton_back_login_screen.setText(_translate("MainWindow", "Back to Login Screen"))
        self.pushButton_exit.setText(_translate("MainWindow", "Exit"))
        self.pushButton_copy_clipboard.setText(_translate("MainWindow", "Copy Password to Clipboard"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "1"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "2"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "3"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "4"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

