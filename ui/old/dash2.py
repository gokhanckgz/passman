# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dash.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(30, 80, 731, 361))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.listWidget = QtWidgets.QListWidget(self.verticalLayoutWidget)
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget)
        self.pushButton_create_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_create_pass.setGeometry(QtCore.QRect(70, 460, 111, 41))
        self.pushButton_create_pass.setObjectName("pushButton_create_pass")
        self.pushButton_regenerate_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_regenerate_pass.setGeometry(QtCore.QRect(200, 459, 111, 41))
        self.pushButton_regenerate_pass.setObjectName("pushButton_regenerate_pass")
        self.pushButton_delete_pass = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_delete_pass.setGeometry(QtCore.QRect(331, 459, 111, 41))
        self.pushButton_delete_pass.setObjectName("pushButton_delete_pass")
        self.pushButton_back_login_screen = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_back_login_screen.setGeometry(QtCore.QRect(20, 20, 141, 41))
        self.pushButton_back_login_screen.setObjectName("pushButton_back_login_screen")
        self.pushButton_exit = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_exit.setGeometry(QtCore.QRect(650, 20, 141, 41))
        self.pushButton_exit.setObjectName("pushButton_exit")
        self.pushButton_copy_clipboard = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_copy_clipboard.setGeometry(QtCore.QRect(580, 460, 181, 41))
        self.pushButton_copy_clipboard.setObjectName("pushButton_copy_clipboard")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_create_pass.setText(_translate("MainWindow", "Create Pass"))
        self.pushButton_regenerate_pass.setText(_translate("MainWindow", "Regenerate Pass"))
        self.pushButton_delete_pass.setText(_translate("MainWindow", "Delete Pass"))
        self.pushButton_back_login_screen.setText(_translate("MainWindow", "Back to Login Screen"))
        self.pushButton_exit.setText(_translate("MainWindow", "Exit"))
        self.pushButton_copy_clipboard.setText(_translate("MainWindow", "Copy Password to Clipboard"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

