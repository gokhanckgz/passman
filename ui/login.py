# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1015, 746)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton_browse_folder = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_browse_folder.setGeometry(QtCore.QRect(60, 460, 151, 61))
        self.pushButton_browse_folder.setObjectName("pushButton_browse_folder")
        self.pushButton_create_db = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_create_db.setGeometry(QtCore.QRect(310, 460, 151, 61))
        self.pushButton_create_db.setObjectName("pushButton_create_db")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(190, 0, 601, 111))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(-10, 220, 241, 61))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(-10, 290, 241, 61))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(-10, 360, 241, 61))
        self.label_4.setObjectName("label_4")
        self.textBrowser_create = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser_create.setGeometry(QtCore.QRect(260, 370, 201, 41))
        self.textBrowser_create.setObjectName("textBrowser_create")
        self.textBrowser_import = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser_import.setGeometry(QtCore.QRect(795, 303, 201, 41))
        self.textBrowser_import.setObjectName("textBrowser_import")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(525, 220, 241, 61))
        self.label_5.setObjectName("label_5")
        self.pushButton_browse_db = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_browse_db.setGeometry(QtCore.QRect(595, 390, 151, 61))
        self.pushButton_browse_db.setObjectName("pushButton_browse_db")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(525, 290, 241, 61))
        self.label_6.setObjectName("label_6")
        self.pushButton_open_db = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_open_db.setGeometry(QtCore.QRect(845, 390, 151, 61))
        self.pushButton_open_db.setObjectName("pushButton_open_db")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(80, 140, 331, 71))
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(610, 153, 331, 41))
        self.label_9.setObjectName("label_9")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(490, 140, 51, 461))
        self.line.setStyleSheet("width:20")
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.lineEdit_key_import = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_key_import.setGeometry(QtCore.QRect(793, 230, 201, 44))
        self.lineEdit_key_import.setObjectName("lineEdit_key_import")
        self.lineEdit_name_create = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_name_create.setGeometry(QtCore.QRect(259, 230, 201, 44))
        self.lineEdit_name_create.setObjectName("lineEdit_name_create")
        self.lineEdit_key_create = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_key_create.setGeometry(QtCore.QRect(258, 300, 201, 44))
        self.lineEdit_key_create.setObjectName("lineEdit_key_create")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1015, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_browse_folder.setText(_translate("MainWindow", "Browse Folder Path"))
        self.pushButton_create_db.setText(_translate("MainWindow", "Create DB"))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:28pt;\">Password Manager Application</span></p></body></html>"))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Name of database</span></p></body></html>"))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Master Key of DB</span></p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Folder Path</span></p></body></html>"))
        self.textBrowser_create.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Cantarell\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.label_5.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Master Key of DB</span></p></body></html>"))
        self.pushButton_browse_db.setText(_translate("MainWindow", "Browse DB"))
        self.label_6.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">DB Path</span></p></body></html>"))
        self.pushButton_open_db.setText(_translate("MainWindow", "Open DB"))
        self.label_8.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:22pt;\">Create new DB</span></p></body></html>"))
        self.label_9.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:22pt;\">Import Existing DB</span></p><p align=\"center\"><br/></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

